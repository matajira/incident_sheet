from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from .models import Incident, IncidentForm
from django.views import generic
from random import uniform

from operator import itemgetter
from itertools import groupby

def show_incidents(request):
    def get_barchart_data(incident_list):
        barchart_labels = []
        barchart_values = []
        get_month = lambda x: x.date.strftime("%Y-%m")
        for month, items in groupby(incident_list, key=get_month):
            barchart_labels.append(str(month))
            barchart_values.append(len(list(items)))
        return (barchart_labels, barchart_values)

    incident_list = Incident.objects.all().order_by('-date')
    barchart_labels, barchart_values = get_barchart_data(incident_list)
    context = {'incident_list': incident_list,
               'barchart_labels': reversed(barchart_labels),
               'barchart_values': reversed(barchart_values)}
    return render(request, 'incident_sheet/incident_list.html', context)
    
class AllIncidentsView(generic.ListView):
    context_object_name = 'incident_list'

    def get_queryset(self):
        return Incident.objects.all().order_by('-date')

class DetailView(generic.DetailView):
    model = Incident

def new_incident(request):
    if request.method == "POST":
        form = IncidentForm(request.POST)
        if form.is_valid():
            form.save()
    form = IncidentForm()
    return render(request, 'incident_sheet/new_incident.html', {'form': form})
