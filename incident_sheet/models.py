from django.db import models
from django.utils import timezone
from django.forms import ModelForm
from django.forms import DateInput, Textarea



class Incident(models.Model):
    def __str__(self):
        return self.title or "NA"
    def to_html_table(self):
        result = ["<td>{}</td>".format(x) for x in [ self.date , self.text ]]
        return " ".join(result)
    date = models.DateField('date')
    title = models.CharField(max_length=500, null=True)
    impact = models.CharField(max_length=2000, null=True)
    description = models.CharField(max_length=2000, null=True)
    investigation = models.CharField(max_length=2000, null=True)
    root_cause = models.CharField(max_length=2000, null=True)
    to_do = models.CharField(max_length=2000, null=True)


class IncidentForm(ModelForm):
    class Meta:
        model = Incident
        fields = '__all__'
        widgets = {
        'date': DateInput(format=('%Y/%m/%d'), attrs={'class':'form-control', 'placeholder':'Select a date', 'type':'date'}),
        'impact': Textarea(attrs={'cols': 50, 'rows': 5}),
        'description': Textarea(attrs={'cols': 50, 'rows': 5}),
        'investigation': Textarea(attrs={'cols': 50, 'rows': 5}),
        'root_cause': Textarea(attrs={'cols': 50, 'rows': 5}),
        'to_do': Textarea(attrs={'cols': 50, 'rows': 5})
    }
