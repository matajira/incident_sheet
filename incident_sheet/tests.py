from django.test import TestCase
from django.urls import reverse
from incident_sheet.models import Incident

class AllIncidentsViewTests(TestCase):
    def test_no_incidents(self):
        response = self.client.get(reverse('all'))
        self.assertEqual(response.status_code, 200)

    def test_one_incidents(self):
        Incident.objects.create(date='2022-02-02', title= 'Big Incident')
        response = self.client.get(reverse('all'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Big Incident")
