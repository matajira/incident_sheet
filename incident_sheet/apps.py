from django.apps import AppConfig


class IncidentSheetConfig(AppConfig):
    name = 'incident_sheet'
