from django.urls import path, re_path

from . import views

urlpatterns = [
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('new/', views.new_incident, name='new_incident'),
    path('', views.show_incidents, name='all'),
]
