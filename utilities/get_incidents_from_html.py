import re
import pandas as pd
import datetime
import sqlite3

import sqlite3

con = sqlite3.connect('./db.sqlite3')

cur = con.cursor()
cur.execute(''' select * from incident_sheet_incident''')
#cur.execute('''PRAGMA table_info(incident_sheet_incident)''')
for i in cur:
    print(i)

import sys
sys.exit()

html_tables = pd.read_html('./incidents2.html')

df = html_tables[0]
print(df)
df.columns = ['reference', 'date', 'title', 'impact', 'description', 'investigation', 'root_cause', 'to_do']
#for i,row in df[1:4].iterrows():
for i,row in df.iterrows():
    print(row['reference'])
    try:
        ref = re.match("IT\#(?P<year>[0-9]{2})(?P<doy>[0-9]{3})(?P<copy>[a-z]?)", row['reference'])
        year = 2000 + int(ref.group('year'))
        doy = int(ref.group('doy'))
        date = (datetime.datetime(year, 1, 1) + datetime.timedelta(doy - 1)).date().isoformat()
        #print(date)
        #print("insert into incident_sheet ('date', 'description', 'impact', 'investigation', 'root_cause', 'title', 'to_do') values (?,?,?,?,?,?,?)")
        #print(( row['date'], row['description'], row['impact'], row['investigation'], row['root_cause'], row['title'], row['to_do']))
        cur.execute("insert into incident_sheet_incident ('date', 'description', 'impact', 'investigation', 'root_cause', 'title', 'to_do') values (?,?,?,?,?,?,?)",  ( date, row['description'], row['impact'], row['investigation'], row['root_cause'], row['title'], row['to_do']))
    except Exception as e:
        print(e)
con.commit()
con.close()
