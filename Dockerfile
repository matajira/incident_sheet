FROM debian:11

RUN apt-get update && apt-get install -y python3-django python3-whitenoise python3-waitress && mkdir /app
COPY db.sqlite3/ /app
COPY django_incident_sheet/ /app/django_incident_sheet/
COPY incident_sheet/ /app/incident_sheet/
COPY manage.py /app
COPY server.py /app

EXPOSE 8080
ENV PYTHONPATH='/app'

RUN cd /app && /usr/bin/python3 manage.py collectstatic && \
    rm -rf /app/incident_sheet/static/ && \
    /usr/bin/python3 manage.py migrate
    
CMD [ "/usr/bin/python3", "/app/server.py"]
