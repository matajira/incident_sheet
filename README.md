# Incident Sheet
![alt text](.images/all_incidents.png "Incident Sheet")
![alt text](.images/create_new.png "Create new Incident")


## About the project
This is a tool to track Incident's at your company or project.

## The Stack
I used packages coming from debian 11. Except graph.js and bootstrap.
I also tested it on Ubuntu 22.04.

```
Python3
python3-django
python3-whitenoise (for serving the static files)
python3-waitress (simple webserver)
graph.js
bootstrap
```
## Running
```
/usr/bin/python3 manage.py collectstatic
/usr/bin/python3 manage.py migrate
/usr/bin/python3 server.py
```
Then, the service will be exposed at localhost:8080

## Author
* Camilo MATAJIRA see www.camilomatajira.com
